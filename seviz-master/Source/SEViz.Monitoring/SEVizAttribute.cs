﻿/*
 * SEViz - Symbolic Execution VIsualiZation
 *
 * SEViz is a tool, which can support the test generation process by
 * visualizing the symbolic execution in a directed graph.
 *
 * Authors: Dávid Honfi <honfi@mit.bme.hu>, Zoltán Micskei
 * <micskeiz@mit.bme.hu>, András Vörös <vori@mit.bme.hu>
 * 
 * Copyright 2015 Budapest University of Technology and Economics (BME)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */

using Microsoft.ExtendedReflection.ComponentModel;
using Microsoft.ExtendedReflection.Emit;
using Microsoft.ExtendedReflection.Interpretation;
using Microsoft.ExtendedReflection.Metadata;
using Microsoft.ExtendedReflection.Metadata.Names;
using Microsoft.ExtendedReflection.Reasoning.ExecutionNodes;
using Microsoft.ExtendedReflection.Symbols;
using Microsoft.ExtendedReflection.Utilities.Safe.IO;
using Microsoft.Pex.Engine.ComponentModel;
using Microsoft.Pex.Engine.Packages;
using Microsoft.Pex.Engine.TestGeneration;
using Microsoft.Pex.Framework.ComponentModel;
using SEViz.Common.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CFG;
using System.Diagnostics;

namespace SEViz.Monitoring
{
    public class ProgramState
    {
        public bool IsRootState;
        public string CurrentMethod;
        public List<string> ParameterList;
        public Dictionary<string, string> CurrentParameterName;

        public static Dictionary<string, List<string>> MethodParameterLists;

        public static ProgramState CreateRootState(string methodName)
        {
            var s = new ProgramState();
            s.IsRootState = true;
            s.CurrentMethod = methodName;
            return s;
        }
        public static ProgramState Create(string expression)
        {
            
            foreach (var fun in ProgramState.MethodParameterLists.Keys)
            {
                if (expression.Contains(fun + "("))
                {

                    int indexOfParaStart = expression.IndexOf(fun + "(");
                    int parLength = expression.IndexOf(")", indexOfParaStart) - (indexOfParaStart + fun.Length + 1);
                    var parameters = expression.Substring(indexOfParaStart + fun.Length + 1, parLength).Split(',');

                    if (parameters.Length == ProgramState.MethodParameterLists[fun].Count)
                    {
                        
                        var state = new ProgramState();
                        state.CurrentMethod = fun;
                        state.ParameterList = ProgramState.MethodParameterLists[fun];
                        state.CurrentParameterName = new Dictionary<string, string>();
                        //throw new Exception(string.Join(",", parameters));
                        for (int i = 0; i<state.ParameterList.Count; i++)
                        {

                            state.CurrentParameterName[state.ParameterList[i]] = parameters[i];
                        }
                        state.IsRootState = false;
                        return state;
                    }
                }

            }
            throw new Exception("State Explosion");
        }
    }
    public class SEVizAttribute : PexComponentElementDecoratorAttributeBase, IPexPathPackage, IPexExplorationPackage
    {
        #region Properties

        public string Name
        {
            get
            {
                return "SEViz";
            }
        }

        private Dictionary<string, string> ParamMap { get; set; }
        private Dictionary<int, SENode> Vertices { get; set; }

        private Stack<ProgramState> States;
        private Dictionary<string, HashSet<string>> MethodParameters{get; set;}
        private Dictionary<string, int> LocationToIndex { get; set; }

        private Dictionary<int, Dictionary<int, SEEdge>> Edges { get; set; }

        public SEGraph Graph { get; private set; }

        private Dictionary<int, Tuple<bool, string>> EmittedTestResult { get; set; }

        private List<string> Z3CallLocations { get; set; }

        private Dictionary<int, Task<string>> PrettyPathConditionTasks { get; set; }

        private Dictionary<int, int> ParentNodes { get; set; }

        private string ClassName { get; set; }
        private string MethodName { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Visualizes symbolic execution.
        /// </summary>
        public SEVizAttribute()
        {
        }

        /// <summary>
        /// Visualizes symbolic execution and marks edges that are pointing in and out of the unit borders.
        /// </summary>
        /// <param name="ClassName">The namespace of the unit to use.</param>
        public SEVizAttribute(string ClassName, string MethodName)
        {
            this.ClassName = ClassName;
            this.MethodName = MethodName;
        }

        #endregion
 
        #region Exploration package
        public object BeforeExploration(IPexExplorationComponent host)
        {

            //// Subscribing to constraint problem handler
            //host.Log.ProblemHandler += (problemEventArgs) =>
            //{
            //    var location = problemEventArgs.FlippedLocation.Method == null ?
            //                    "" :
            //                    (problemEventArgs.FlippedLocation.Method.FullName + ":" + problemEventArgs.FlippedLocation.Offset);

            //    Z3CallLocations.Add(location);
            //};

            Program p = new CFG.Program(ClassName, MethodName);

            ProgramState.MethodParameterLists = p.MethodParameters;
            Graph = p.MainGraph;
            
            // Subscribing to test emitting handler
            host.Log.GeneratedTestHandler += (generatedTestEventArgs) =>
            {
                // Getting the number of the corresponding run
                var run = generatedTestEventArgs.GeneratedTest.Run;

                // Storing the result of the test (this is called before AfterRun)
                EmittedTestResult.Add(run, new Tuple<bool, string>(generatedTestEventArgs.GeneratedTest.IsFailure, generatedTestEventArgs.GeneratedTest.MethodCode));
            };

            // Checking if temporary SEViz folder exists
            if (!Directory.Exists(Path.GetTempPath() + "PexViz"))
            {
                var dir = Directory.CreateDirectory(Path.GetTempPath() + "PexViz");

            }
            
            return null;
        }

        public void AfterExploration(IPexExplorationComponent host, object data)
        {

            foreach (var vertex in Vertices.Values)
            {
                // Modifying shape based on Z3 calls
                //if (Z3CallLocations.Contains(vertex.MethodName + ":" + vertex.ILOffset)) vertex.Shape = SENode.NodeShape.Ellipse;
                // Adding the path condition
                if (PrettyPathConditionTasks.ContainsKey(vertex.Id))
                {
                    var t = PrettyPathConditionTasks[vertex.Id];
                    t.Wait();
                    vertex.PathCondition = t.Result;
                }
            }

            foreach (var vertex in Vertices.Values)
            {
                // Adding the incremental path condition
                if (ParentNodes.ContainsKey(vertex.Id))
                {
                    vertex.IncrementalPathCondition = CalculateIncrementalPathCondition(vertex.PathCondition, Vertices[ParentNodes[vertex.Id]].PathCondition);
                }
                else
                {
                    // If the node is the first one (has no parents), then the incremental equals the full PC
                    vertex.IncrementalPathCondition = vertex.PathCondition;
                }

                if (vertex.GenerateTestCode == "")
                {
                    vertex.DisplayName = vertex.IncrementalPathCondition;
                }
                //vertex.DisplayName = vertex.IncrementalPathCondition;


            }

            // Adding vertices and edges to the graph
            Graph.AddVertexRange(Vertices.Values);
            foreach (var edgeDictionary in Edges.Values) Graph.AddEdgeRange(edgeDictionary.Values);

            foreach(var vertex in Graph.Vertices)
            {
                if (vertex.Color == SENode.NodeColor.White)
                {
                    string[] tokens = SplitByOperator(vertex.DisplayName);
                    foreach (string token in tokens)
                    {
                        if (MethodParameters[MethodName].Contains(token))
                        {
                            vertex.Color = SENode.NodeColor.Orange;
                            break;
                        }
                    }

                }


                if (vertex.Color == SENode.NodeColor.Green && vertex.FlipCount == 0)
                {
                    vertex.Color = SENode.NodeColor.Red;
                }
            }

            // Getting the temporary folder
            var tempDir = Path.GetTempPath() + "PexViz\\";

            // Serializing the graph into graphml
            SEGraph.Serialize(Graph, tempDir);

        }

        #endregion

        #region Run package

        public object BeforeRun(IPexPathComponent host)
        {

            return null;
        }

        private string[] SplitByOperator(string expr)
        {
            List<string> tokens = new List<string>();
            if(expr == null || expr == "" )
            {
                return new string[] { };
            }
            return expr.Trim().Split(' ');
        }

        private string[] SplitByLogicalOperator(string expr)
        {
            List<string> tokens = new List<string>();
            if (expr == null || expr == "")
            {
                return new string[] { };
            }
            return expr.Trim().Split(new string[] { "||", "&&" }, StringSplitOptions.None);
        }

        private bool isValidityChecking(IExecutionNode curnode, string expression)
        {
            if (curnode.InCodeBranch.IsCheck)
            {
                return true;
            }
            if(expression.Trim().Length == 0)
            {
                return true;
            }

            return false;
        }

        public enum ExpressionResult
        {
            IsSameTrue,
            IsSameFalse,
            IsDifferent
        }

        private bool IsSymbolicValue(string token, string methodName)
        {
            token = token.Trim();
            var currentState = States.Peek();
            if (currentState.IsRootState == false)
            {
                if (currentState.ParameterList.Contains(token))
                {
                    token = currentState.CurrentParameterName[token];
                }
            }

            if (ParamMap.ContainsKey(token))
            {
                return false;
            }
            foreach (string parameter in MethodParameters[methodName])
            {
                if (token == parameter || token.StartsWith(parameter + ".") || token.StartsWith(parameter + "["))
                {
                    return true;
                }
            }
            return false;
        }

        private string[] IsFunctionCall(string expression)
        {
            foreach (var fun in ProgramState.MethodParameterLists.Keys)
            {
                if (expression.Contains(fun+"("))
                {
                    int indexOfParaStart = expression.IndexOf(fun + "(");
                    int parLength = expression.IndexOf(")", indexOfParaStart) - (indexOfParaStart + fun.Length + 1);
                    var parameters = expression.Substring(indexOfParaStart + fun.Length + 1, parLength).Split(',');
                    if (parameters.Length == ProgramState.MethodParameterLists[fun].Count)
                    {
                        return parameters;
                    }
                }

            }
            return null;
        }
        private bool IsSameSymbolicValue(string token, string token2, string methodName)
        {
            foreach (string parameter in MethodParameters[methodName])
            {
                if (token == parameter || token.StartsWith(parameter + ".") || token.StartsWith(parameter + "["))
                {
                    if(token == parameter && token == token2)
                    {
                        return true;
                    }
                    if(token.StartsWith(parameter + ".") && token.Split('.')[0] == token2.Split('.')[0])
                    {
                        return true;
                    }
                    if(token.StartsWith(parameter + "[") && token.Split('[')[0] == token2.Split('[')[0])
                    {
                        return true;
                    }
                    return false;
                }
            }
            return false;
        }

        private bool IsDifferentOperator(string token, string token2)
        {
            string[] ops = new string[] { "<=",">=","==","<",">","!="};
            if(token != token2 && ops.Contains(token))
            {
                return true;
            }
            return false;
        }

        private string GetOppositeOperator(string op)
        {
            if(op == ">=")
            {
                return "<";
            }
            if(op == "<=")
            {
                return ">";
            }
            if(op == ">")
            {
                return "<";
            }
            if(op == "<")
            {
                return ">";
            }
            return op;
        }
        private string[] ReorderTokens(string[] tokens,string methodName)
        {
            //assume length of tokens is 3
            if(tokens.Length == 3)
            {
                if (IsSymbolicValue(tokens[2],methodName))
                {
                    string temp = tokens[0];
                    tokens[0] = tokens[2];
                    tokens[2] = temp;
                    tokens[1] = GetOppositeOperator(tokens[1]);
                }
            }
            return tokens;
        }
        private ExpressionResult isSameExpression(string expression1, string expression2, string methodName)
        {
            string[] tokens1 = SplitByOperator(expression1);
            string[] tokens2 = SplitByOperator(expression2);

            var currentState = States.Peek();
            if (currentState.IsRootState == false)
            {
                //Change variable name according to state
                for (int i = 0; i < tokens1.Length; i++)
                {
                    
                    if (currentState.ParameterList.Contains(tokens1[i]))
                    {
                        //throw new Exception(String.Join(",",currentState.CurrentParameterName.Values));
                        tokens1[i] = currentState.CurrentParameterName[tokens1[i]];
                        
                    }
                }
            }
            tokens1 = ReorderTokens(tokens1, methodName);
            tokens2 = ReorderTokens(tokens2, methodName);
            //throw new Exception(String.Join(" ",tokens1) + ":" + String.Join(" ", tokens2));
            if(tokens1.Length != tokens2.Length)
            {
                
                return ExpressionResult.IsDifferent;
            }

            //if(tokens2[2] == "5")
            //{
            //    throw new Exception(string.Join(",",tokens1)+";"+ string.Join(",", tokens2));
            //}
            for (int i = 0; i < tokens1.Length; i++)
            {
                if(tokens1[i] == tokens2[i])
                {
                    continue;
                }
                if(IsSymbolicValue(tokens1[i],methodName))
                {
                    if (IsSameSymbolicValue(tokens1[i], tokens2[i], methodName))
                    {
                        continue;
                    }
                    else
                    {
                        
                        //throw new IndexOutOfRangeException("differnt: "+string.Join(",", tokens1) + string.Join(",", tokens2));
                        return ExpressionResult.IsDifferent;
                    }
                }
                if (IsDifferentOperator(tokens1[i], tokens2[i]))
                {
                    //throw new IndexOutOfRangeException(string.Join(",", tokens1) + string.Join(",", tokens2));
                    return ExpressionResult.IsSameFalse;
                }

            }
            return ExpressionResult.IsSameTrue;

        }

        private void AddNodeToPossible(List<SENode> possible, SENode toAdd, string methodName,int depth = 0)
        {
            if(depth > 3)
            {
                return;
            }
            string[] tokens = SplitByOperator(toAdd.DisplayName);
            foreach(string token in tokens)
            {

                    if(IsSymbolicValue(token,methodName))
                    {
                        possible.Add(toAdd);
                        return;
                    }
            }
            toAdd.Color = SENode.NodeColor.White;
            var parameters = IsFunctionCall(toAdd.DisplayName);
            if (parameters != null)
            {
                var newstate = ProgramState.Create(toAdd.DisplayName);
                States.Push(newstate);
            }
            foreach(var neighbor in Graph.GetNeighborNodes(toAdd))
            {
                AddNodeToPossible(possible, neighbor, methodName, depth + 1);
            }
        }

        private void UpdateSymbolicValueState(string ipc, string methodName)
        {
            string[] chunks = SplitByLogicalOperator(ipc);
            foreach(string chunk in chunks)
            {
                if (chunk.Contains("=="))
                {
                    string[] tokens = SplitByOperator(chunk);
                    if (tokens.Length == 3) {
                        if (IsSymbolicValue(tokens[0], methodName) && !IsSymbolicValue(tokens[2], methodName))
                        {
                            ParamMap[tokens[0].Trim()] = tokens[2].Trim();
                        }
                        else if (IsSymbolicValue(tokens[2], methodName) && !IsSymbolicValue(tokens[0], methodName))
                        {
                            ParamMap[tokens[2].Trim()] = tokens[0].Trim();
                        }
                    }
                }
            }
        }
        public void AfterRun(IPexPathComponent host, object data)
        {
            ParamMap = new Dictionary<string, string>();
            var runId = host.ExplorationServices.Driver.Runs;
            var nodesInPath = host.PathServices.CurrentExecutionNodeProvider.ReversedNodes.Reverse().ToArray();

            var prevCondtion = "";
            List<SENode> possiblecur = new List<SENode> { Graph.Edges.First().Target };
            Graph.root.Shape = SENode.NodeShape.Ellipse;
            Graph.root.Color = SENode.NodeColor.Blue;
            if (!MethodParameters.ContainsKey(MethodName))
            {
                MethodParameters[MethodName] = new HashSet<string>();
                var parameters = nodesInPath[0].CodeLocation.Method.Parameters.Skip(2);
                foreach (var parameter in parameters)
                {
                    MethodParameters[this.MethodName].Add(parameter.Name);
                }
            }

            var state = ProgramState.CreateRootState(MethodName);
            States = new Stack<ProgramState>();
            States.Push(state);
            for (int i = 0; i < nodesInPath.Length; i++)
            {
                var curnode = nodesInPath[i];
                var codeLocation = MapToSourceCodeLocationString(host, curnode);
                // Adding path condition tasks and getting the required services
                TermEmitter termEmitter = new TermEmitter(host.GetService<TermManager>());
                SafeStringWriter safeStringWriter = new SafeStringWriter();
                IMethodBodyWriter methodBodyWriter = host.GetService<IPexTestManager>().Language.CreateBodyWriter(safeStringWriter, VisibilityContext.Private, 2000);
                var t = PrettyPrintPathCondition(termEmitter, methodBodyWriter, safeStringWriter, curnode);
                t.Wait();
                var incrementalPC = CalculateIncrementalPathCondition(t.Result, prevCondtion);

                List<SENode> newcur = new List<SENode>();
                if (possiblecur.Count == 1 && possiblecur[0].DisplayName == "true")
                {
                    possiblecur[0].Color = SENode.NodeColor.White;
                    possiblecur[0] = Graph.GetNeighborNodes(possiblecur[0]).First();
                    continue;
                }

                if(!isValidityChecking(curnode, incrementalPC))
                {
                foreach (var cur in possiblecur)
                {
                    IEnumerable<SEEdge> outEdges = null;
                    if (Graph.TryGetOutEdges(cur, out outEdges))
                    {
                        ExpressionResult exprRet = isSameExpression(cur.DisplayName, incrementalPC, MethodName);
                        if (exprRet == ExpressionResult.IsSameTrue)
                        {
                            cur.Color = SENode.NodeColor.Green;
                            //file.WriteLine("True: " + cur.DisplayName + " === " + incrementalPC);
                            IEnumerable<SEEdge> edgess = outEdges.Where(x => x.DisplayName == "True");
                            cur.FlipCount += curnode.FlipCount;
                            if (edgess.Count() == 0)
                            {
                                if (i == nodesInPath.Length - 1)
                                {
                                    return;
                                }
                                IEnumerable<SEEdge> LoopEdges = outEdges.Where(x => x.DisplayName == "Loop");
                                IEnumerable<SEEdge> ReturnEdges = outEdges.Where(x => x.DisplayName == "Return");
                                if (LoopEdges.Count() != 0 || ReturnEdges.Count() != 0)
                                {
                                    if(LoopEdges.Count() != 0)
                                        AddNodeToPossible(newcur, LoopEdges.Single().Target, MethodName);
                                    if(ReturnEdges.Count() != 0)
                                        {
                                            States.Pop();
                                            AddNodeToPossible(newcur, ReturnEdges.Single().Target, MethodName);
                                        }
                                        
                                }
                                else
                                    throw new IndexOutOfRangeException("No True Path for " + incrementalPC + ", prev is " + prevCondtion + ", cur is " + cur.DisplayName);
                            }
                            else
                            {

                                newcur = new List<SENode>();
                                AddNodeToPossible(newcur, edgess.Single().Target, MethodName);
                                break;
                            }
                        }
                        else if (exprRet == ExpressionResult.IsSameFalse)
                        {
                            //file.WriteLine("False: " + cur.DisplayName + " === " + incrementalPC);
                            IEnumerable<SEEdge> edgess = outEdges.Where(x => x.DisplayName == "False");
                            cur.FlipCount += curnode.FlipCount;
                            if (edgess.Count() == 0)
                            {
                                if (i == nodesInPath.Length - 1)
                                {
                                    return;
                                }
                                IEnumerable<SEEdge> LoopEdges = outEdges.Where(x => x.DisplayName == "Loop");
                                IEnumerable<SEEdge> ReturnEdges = outEdges.Where(x => x.DisplayName == "Return");
                                if (LoopEdges.Count() != 0 || ReturnEdges.Count() != 0)
                                {
                                    if (LoopEdges.Count() != 0)
                                        AddNodeToPossible(newcur, LoopEdges.Single().Target, MethodName);
                                    if (ReturnEdges.Count() != 0)
                                        {
                                            States.Pop();
                                            AddNodeToPossible(newcur, ReturnEdges.Single().Target, MethodName);
                                        }
                                    }
                                else
                                    throw new IndexOutOfRangeException("No False Path for " + incrementalPC + ", prev is " + prevCondtion + ", cur is " + cur.DisplayName);
                            }
                            else
                            {

                                newcur = new List<SENode>();
                                AddNodeToPossible(newcur, edgess.Single().Target, MethodName);
                                break;
                            }

                        }
                        else if (exprRet == ExpressionResult.IsDifferent)
                        {
                                //throw new IndexOutOfRangeException("Different: " + incrementalPC + ", prev is " + prevCondtion + ", cur is " + cur.DisplayName);
                            }

                    }
                }

                if (newcur.Count == 0)
                {
                    break;
                }

                possiblecur = newcur;
            }
                prevCondtion = t.Result;
                UpdateSymbolicValueState(incrementalPC,MethodName);

            }
            
        }

        public void AfterRunb(IPexPathComponent host, object data)
        {
            var runId = host.ExplorationServices.Driver.Runs;
            var nodesInPath = host.PathServices.CurrentExecutionNodeProvider.ReversedNodes.Reverse().ToArray();
            using (System.IO.StreamWriter file = new StreamWriter(File.Open(Path.GetTempPath() + "PexViz\\output.txt", System.IO.FileMode.Append)))
            {
                file.WriteLine("---start" + runId + "---");
                var prevCondtion = "";
                for (int i = 0; i < nodesInPath.Length; i++)
                {
                    var curnode = nodesInPath[i];
                    // Adding path condition tasks and getting the required services
                    TermEmitter termEmitter = new TermEmitter(host.GetService<TermManager>());
                    SafeStringWriter safeStringWriter = new SafeStringWriter();
                    IMethodBodyWriter methodBodyWriter = host.GetService<IPexTestManager>().Language.CreateBodyWriter(safeStringWriter, VisibilityContext.Private, 2000);
                    var t = PrettyPrintPathCondition(termEmitter, methodBodyWriter, safeStringWriter, curnode);
                    t.Wait();
                    if(curnode.InCodeBranch.IsCheck == false)
                    file.WriteLine(CalculateIncrementalPathCondition(t.Result, prevCondtion)+", FlipCount: "+ curnode.FlipCount);
                    prevCondtion = t.Result;
                }
                file.WriteLine("---end---");
            }
        }
        public void AfterRunBackup(IPexPathComponent host, object data)
        {
            var runId = host.ExplorationServices.Driver.Runs;
            // Getting the executions nodes in the current path
            var nodesInPath = host.PathServices.CurrentExecutionNodeProvider.ReversedNodes.Reverse().ToArray();
            int firstLocation = nodesInPath[0].CodeLocation.SerializableName.GetHashCode();
            if (!Vertices.ContainsKey(firstLocation))
            {
                Vertices.Add(firstLocation,new SENode(firstLocation, null, null, null, null, null, false));
                // Adding path condition tasks and getting the required services
                TermEmitter termEmitter = new TermEmitter(host.GetService<TermManager>());
                SafeStringWriter safeStringWriter = new SafeStringWriter();
                IMethodBodyWriter methodBodyWriter = host.GetService<IPexTestManager>().Language.CreateBodyWriter(safeStringWriter, VisibilityContext.Private, 2000);
                PrettyPathConditionTasks.Add(firstLocation, PrettyPrintPathCondition(termEmitter, methodBodyWriter, safeStringWriter, nodesInPath[0]));

            }

            for (int i = 1; i < nodesInPath.Length; i++)
            {
                var curnode = nodesInPath[i];
                var prevnode = nodesInPath[i - 1];

                int curlocation = curnode.CodeLocation.SerializableName.GetHashCode();
                int prevlocation = prevnode.CodeLocation.SerializableName.GetHashCode();

                SENode curvertex = null;
                SENode prevvertex = Vertices[prevlocation];

                if (Vertices.ContainsKey(curlocation)/* && !(i == nodesInPath.Length - 1)*/)
                {
                    curvertex = Vertices[curlocation];
                }
                else
                {
                    //if (i == nodesInPath.Length - 1)
                    //{
                    //    if (!EmittedTestResult.ContainsKey(runId))
                    //    {
                    //        //Edges[curlocation].Last().Value.Target.sameTestCount += 1;
                    //        return;
                    //    }
                    //    curlocation = curlocation + runId;
                    //}
                    curvertex = new SENode(curlocation, "dummy", "dummy", "dummy", "dummy", "dummy", false);
                    Vertices.Add(curlocation, curvertex);
                    curvertex.Color = SENode.NodeColor.White;
                    // Adding the method name this early in order to color edges
                    string methodName = null;
                    int offset = 0;
                    if (curnode.CodeLocation.Method == null)
                    {
                        if (curnode.InCodeBranch.Method != null)
                        {
                            methodName = curnode.InCodeBranch.Method.FullName;
                        }
                    }
                    else
                    {
                        methodName = curnode.CodeLocation.Method.FullName;
                        offset = curnode.CodeLocation.Offset;
                    }
                    // Setting the method name
                    curvertex.MethodName = methodName;
                    // Setting the offset
                    curvertex.ILOffset = offset;

                    // Adding source code mapping
                    curvertex.SourceCodeMappingString = MapToSourceCodeLocationString(host, curnode);

                    // Setting the border based on mapping existence
                    curvertex.Border = curvertex.SourceCodeMappingString == null ? SENode.NodeBorder.Single : SENode.NodeBorder.Double;

                    curvertex.Shape = SENode.NodeShape.Rectangle;

                    // Adding path condition tasks and getting the required services
                    TermEmitter termEmitter = new TermEmitter(host.GetService<TermManager>());
                    SafeStringWriter safeStringWriter = new SafeStringWriter();
                    IMethodBodyWriter methodBodyWriter = host.GetService<IPexTestManager>().Language.CreateBodyWriter(safeStringWriter, VisibilityContext.Private, 2000);
                    PrettyPathConditionTasks.Add(curvertex.Id, PrettyPrintPathCondition(termEmitter, methodBodyWriter, safeStringWriter, curnode));

                    //if (i == nodesInPath.Length - 1)
                    //{
                    //    if (!EmittedTestResult.ContainsKey(runId))
                    //    {
                    //        curvertex.Color = SENode.NodeColor.Orange;

                    //    }
                    //    else
                    //    {
                    //        var id = "MAGIC".GetHashCode() + runId;
                    //        var resultvertex = new SENode(id, "dummy", "dummy", "dummy", "dummy", "dummy", false);
                    //        Vertices.Add(id, resultvertex);

                    //        if (EmittedTestResult[runId].Item1)
                    //        {
                    //            resultvertex.Color = SENode.NodeColor.Red;
                    //        }
                    //        else
                    //        {
                    //            resultvertex.Color = SENode.NodeColor.Green;
                    //        }
                    //        resultvertex.Shape = SENode.NodeShape.Ellipse;
                    //        resultvertex.GenerateTestCode = EmittedTestResult[runId].Item2;
                    //        resultvertex.DisplayName = "Test " + runId.ToString();

                    //        var resultedge = new SEEdge(new Random().Next(), curvertex, resultvertex);

                    //        if (Edges.ContainsKey(curlocation))
                    //        {
                    //            if (!Edges[curlocation].ContainsKey(id))
                    //            {
                    //                Edges[curlocation].Add(id, resultedge);
                    //            }
                    //        }
                    //        else
                    //        {
                    //            Edges.Add(curlocation, new Dictionary<int, SEEdge>());
                    //            Edges[curlocation].Add(id, resultedge);
                    //        }
                    //    }
                    //}
                }
                if (i == nodesInPath.Length - 1)
                {
                    if (!EmittedTestResult.ContainsKey(runId))
                    {
                        if(curvertex.Color == SENode.NodeColor.White)
                        curvertex.Color = SENode.NodeColor.Orange;

                    }
                    else
                    {

                        if (EmittedTestResult[runId].Item1)
                        {
                            curvertex.Color = SENode.NodeColor.Red;
                        }
                        else
                        {
                            curvertex.Color = SENode.NodeColor.Green;
                        }
                        curvertex.Shape = SENode.NodeShape.Ellipse;
                        curvertex.GenerateTestCode = EmittedTestResult[runId].Item2;
                        curvertex.DisplayName = "Tests";
                    }
                }
                curvertex.FlipCount += curnode.FlipCount;
                curvertex.Status = curnode.ExhaustedReason.ToString();

                
                curvertex.Runs += (runId + ";");


                

                var edge = new SEEdge(new Random().Next(), prevvertex, curvertex);

                if (Edges.ContainsKey(prevlocation))
                {
                    if (!Edges[prevlocation].ContainsKey(curlocation))
                    {
                        Edges[prevlocation].Add(curlocation, edge);
                    }
                }
                else
                {
                    Edges.Add(prevlocation, new Dictionary<int, SEEdge>());
                    Edges[prevlocation].Add(curlocation, edge);
                }

                if (!ParentNodes.ContainsKey(curvertex.Id))
                {
                    ParentNodes.Add(curvertex.Id, prevvertex.Id);
                }

            }
            //// Getting the sequence id of the current run
            //var runId = host.ExplorationServices.Driver.Runs;

            //// Iterating over the nodes in the path
            //foreach (var node in nodesInPath)
            //{

            //    string location = MapToSourceCodeLocationString(host, node);
            //    int id = -1;
            //    if (LocationToIndex.ContainsKey(location))
            //    {
            //        id = LocationToIndex[location];
            //    }else
            //    {
            //        id = LocationToIndex[location] = node.UniqueIndex;
            //    }
            //    var vertex = new SENode(id, null, null, null, null, null, false);

            //    // Adding the method name this early in order to color edges
            //    string methodName = null;
            //    int offset = 0;
            //    if (node.CodeLocation.Method == null)
            //    {
            //        if (node.InCodeBranch.Method != null)
            //        {
            //            methodName = node.InCodeBranch.Method.FullName;
            //        }
            //    }
            //    else
            //    {
            //        methodName = node.CodeLocation.Method.FullName;
            //        offset = node.CodeLocation.Offset;
            //    }
            //    // Setting the method name
            //    vertex.MethodName = methodName;

            //    // Setting the offset
            //    vertex.ILOffset = offset;

            //    var nodeIndex = nodesInPath.ToList().IndexOf(node);
            //    if (nodeIndex > 0)
            //    {
            //        var prevNode = nodesInPath[nodeIndex - 1];
            //        string prevLocation = MapToSourceCodeLocationString(host, prevNode);
            //        int prevId = LocationToIndex[prevLocation];
            //        // If there is no edge between the previous and the current node
            //        if (!(Edges.ContainsKey(prevId) && Edges[prevId].ContainsKey(id)))
            //        {

            //            var prevVertex = Vertices[prevId];

            //            var edge = new SEEdge(new Random().Next(), prevVertex, vertex);

            //            Dictionary<int, SEEdge> outEdges = null;
            //            if (Edges.TryGetValue(prevId, out outEdges))
            //            {
            //                outEdges.Add(id, edge);
            //            }
            //            else
            //            {
            //                Edges.Add(prevId, new Dictionary<int, SEEdge>());
            //                Edges[prevId].Add(id, edge);
            //            }

            //            // Edge coloring based on unit border detection
            //            if (UnitNamespace != null)
            //            {
            //                // Checking if pointing into the unit from outside
            //                if (!(prevVertex.MethodName.StartsWith(UnitNamespace + ".") || prevVertex.MethodName.Equals(UnitNamespace)) && (vertex.MethodName.StartsWith(UnitNamespace + ".") || vertex.MethodName.Equals(UnitNamespace)))
            //                {
            //                    edge.Color = SEEdge.EdgeColor.Green;
            //                }

            //                // Checking if pointing outside the unit from inside
            //                if ((prevVertex.MethodName.StartsWith(UnitNamespace + ".") || prevVertex.MethodName.Equals(UnitNamespace)) && !(vertex.MethodName.StartsWith(UnitNamespace + ".") || vertex.MethodName.Equals(UnitNamespace)))
            //                {
            //                    edge.Color = SEEdge.EdgeColor.Red;
            //                }
            //            }
            //        }
            //    }

            //    // If the node is new then it is added to the list and the metadata is filled
            //    if (!Vertices.ContainsKey(id))
            //    {

            //        Vertices.Add(id, vertex);

            //        // Adding source code mapping
            //        vertex.SourceCodeMappingString = MapToSourceCodeLocationString(host, node);

            //        // Setting the border based on mapping existence
            //        vertex.Border = vertex.SourceCodeMappingString == null ? SENode.NodeBorder.Single : SENode.NodeBorder.Double;

            //        // Setting the color
            //        if (nodesInPath.LastOrDefault() == node)
            //        {
            //            if (!EmittedTestResult.ContainsKey(runId))
            //            {
            //                vertex.Color = SENode.NodeColor.Orange;
            //            }
            //            else
            //            {
            //                if (EmittedTestResult[runId].Item1)
            //                {
            //                    vertex.Color = SENode.NodeColor.Red;
            //                }
            //                else
            //                {
            //                    vertex.Color = SENode.NodeColor.Green;
            //                }
            //                vertex.GenerateTestCode = EmittedTestResult[runId].Item2;
            //            }
            //        }
            //        else
            //        {
            //            vertex.Color = SENode.NodeColor.White;
            //        }

            //        // Setting the default shape
            //        vertex.Shape = SENode.NodeShape.Rectangle;

            //        // Adding path condition tasks and getting the required services
            //        TermEmitter termEmitter = new TermEmitter(host.GetService<TermManager>());
            //        SafeStringWriter safeStringWriter = new SafeStringWriter();
            //        IMethodBodyWriter methodBodyWriter = host.GetService<IPexTestManager>().Language.CreateBodyWriter(safeStringWriter, VisibilityContext.Private, 2000);
            //        PrettyPathConditionTasks.Add(vertex.Id, PrettyPrintPathCondition(termEmitter, methodBodyWriter, safeStringWriter, node));

            //        // Setting the status
            //        vertex.Status = node.ExhaustedReason.ToString();

            //        // Collecting the parent nodes for the later incremental path condition calculation
            //        if (nodeIndex > 0)
            //        {
            //            var prevNode = nodesInPath[nodeIndex - 1];
            //            string prevLocation = MapToSourceCodeLocationString(host, prevNode);
            //            int prevId = LocationToIndex[prevLocation];
            //            ParentNodes.Add(vertex.Id, prevId);
            //        }

            //    }

            //    // Adding the Id of the run

            //    Vertices[id].Runs += (runId + ";");

            //}
            //throw new System.ArgumentException("qiezi", "original");
        }

        #endregion

        public void Initialize(IPexExplorationEngine host)
        {
            Graph = new SEGraph();
            Vertices = new Dictionary<int, SENode>();
            Edges = new Dictionary<int, Dictionary<int, SEEdge>>();
            EmittedTestResult = new Dictionary<int, Tuple<bool, string>>();
            Z3CallLocations = new List<string>();
            PrettyPathConditionTasks = new Dictionary<int, Task<string>>();
            ParentNodes = new Dictionary<int, int>();
            LocationToIndex = new Dictionary<string, int>();
            
            MethodParameters = new Dictionary<string, HashSet<string>>();

        }
        
        public void Load(IContainer pathContainer)
        {
        }

        protected override void Decorate(Name location, IPexDecoratedComponentElement host)
        {
            host.AddExplorationPackage(location, this);
            host.AddPathPackage(location, this);
        }

        #region Private methods 

        /// <summary>
        /// Maps the execution node to source code location string.
        /// </summary>
        /// <param name="host">Host of the Pex Path Component</param>
        /// <param name="node">The execution node to map</param>
        /// <returns>The source code location string in the form of [documentlocation]:[linenumber]</returns>
        private string MapToSourceCodeLocationString(IPexPathComponent host, IExecutionNode node)
        {
            try
            {
                
                var symbolManager = host.GetService<ISymbolManager>();
                var sourceManager = host.GetService<ISourceManager>();
                MethodDefinitionBodyInstrumentationInfo nfo;
                if (node.CodeLocation.Method == null)
                {
                    if (node.InCodeBranch.Method == null) { return null; }
                    else { node.InCodeBranch.Method.TryGetBodyInstrumentationInfo(out nfo); }
                }
                else { node.CodeLocation.Method.TryGetBodyInstrumentationInfo(out nfo); }
                SequencePoint point;
                int targetOffset;
                nfo.TryGetSourceOffset(node.InCodeBranch.BranchLabel, out targetOffset);
                if (symbolManager.TryGetSequencePoint(node.CodeLocation.Method == null ? node.InCodeBranch.Method : node.CodeLocation.Method, node.CodeLocation.Method == null ? targetOffset : node.CodeLocation.Offset, out point))
                {
                    return point.Document + ":" + point.Line;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                // TODO Exception handling
                return null;
            }
        }

        /// <summary>
        /// Pretty prints the path condition of the execution node.
        /// </summary>
        /// <param name="host">Host of the Pex Path Component</param>
        /// <param name="node">The execution node to map</param>
        /// <returns>The pretty printed path condition string</returns>
        private Task<string> PrettyPrintPathCondition(TermEmitter emitter, IMethodBodyWriter mbw, SafeStringWriter ssw, IExecutionNode node)
        {
            var task = Task.Factory.StartNew(() =>
            {
                string output = "";
                try
                {
                    if (node.GetPathCondition().Conjuncts.Count != 0)
                    {

                        if (emitter.TryEvaluate(node.GetPathCondition().Conjuncts, 2000, mbw)) // TODO Perf leak
                        {
                            for (int i = 0; i < node.GetPathCondition().Conjuncts.Count - 1; i++)
                            {
                                mbw.ShortCircuitAnd();
                            }

                            mbw.Statement();
                            var safeString = ssw.ToString();
                            output = safeString.Remove(ssw.Length - 3);
                        }
                    }
                }
                catch (Exception) { }
                return output;
            });
            return task;

        }

        /// <summary>
        /// Calculates the incremental path condition of a node
        /// </summary>
        /// <param name="pc">The path condition of the current node</param>
        /// <param name="prevPc">The path condition of the previous node</param>
        /// <returns>The incremental path condition of the node</returns>
        private string CalculateIncrementalPathCondition(string pc, string prevPc)
        {
            var remainedLiterals = new List<string>();

            var splittedCondition = pc.Split(new string[] { "&& " }, StringSplitOptions.None);
            var prevSplittedCondition = prevPc.Split(new string[] { "&& " }, StringSplitOptions.None);

            var currentOrdered = splittedCondition.OrderBy(c => c);
            var prevOrdered = prevSplittedCondition.OrderBy(c => c);

            remainedLiterals = currentOrdered.Except(prevOrdered).ToList(); // TODO Perf leak

            Parallel.For(1, 3, (i) =>
            {
                Parallel.ForEach(prevOrdered, c =>
                {
                    var incrementedLiteral = Regex.Replace(c, "s\\d+", n => "s" + (int.Parse(n.Value.TrimStart('s')) + i).ToString());
                    remainedLiterals.Remove(incrementedLiteral);
                });
            });

            var remainedBuilder = new StringBuilder();
            foreach (var literal in remainedLiterals)
            {
                if (remainedLiterals.IndexOf(literal) != 0)
                    remainedBuilder.Append(" && ");
                remainedBuilder.Append(literal);
            }
            return remainedBuilder.ToString();
        }
        #endregion
    }
}