﻿using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SEViz.Monitoring;
using Examples;
using System.Reflection;

namespace Examples.Tests
{
    [PexClass(typeof(Program))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class ProgramTest
    {

        [PexMethod]
        [SEViz("Examples\\Program.cs", "WhileLoop")]
        public int WhileLoopTest(
            [PexAssumeUnderTest]Program target,
            int[] a,
            int b,
            char[] c
        )
        {
            int result = target.WhileLoop(a, b, c);
            return result;
            // TODO: add assertions to method ATest.FooTest(A, Boolean, Int32)
        }


        [PexMethod(MaxRunsWithoutNewTests = 1000)]
        [SEViz("Examples\\Program.cs", "TestLoop1")]
        public bool TestLoop1Test(
            [PexAssumeUnderTest]Program target,
            int x,
            int[] y
        )
        {
            bool result = target.TestLoop1(x, y);
            return result;
            // TODO: add assertions to method ProgramTest.TestLoop1(Program, Int32, Int32[])
        }


        [PexMethod]
        [SEViz("Examples\\Program.cs", "MultipleMethods")]
        public int MultipleMethodsTest(
            [PexAssumeUnderTest]Program target,
            int a,
            int b
        )
        {
            int result = target.MultipleMethods(a, b);
            return result;
            // TODO: add assertions to method ATest.FooTest(A, Boolean, Int32)
        }

        [PexMethod]
        [SEViz("Examples\\Program.cs", "MultipleMethodsWithPostCall")]
        public int MultipleMethodsWithPostCallTest(
            [PexAssumeUnderTest]Program target,
            int a,
            int b
        )
        {
            int result = target.MultipleMethodsWithPostCall(a, b);
            return result;
            // TODO: add assertions to method ATest.FooTest(A, Boolean, Int32)
        }
    }
}
