﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples
{
    public class Program
    {
        public int foo(int x)
        {
            if(x != 5)
            {
                if (x != 6)
                    return 1;
                else
                    return x;
            }else
            {
                return 2;
            }
        }

        public int fun(int x)
        {
            if (x == 3)
            {
                return 3;
            }
            else
            {
                return x;
            }
        }
        public int MultipleMethods(int a, int b)
        {
            if(a > 0)
            {
                if(foo(b) == 1)
                {
                    a++;
                }else
                {
                    b++;
                }
            }else
            {
                if(fun(a) == 3)
                {
                    return a + b;
                }
            }
            return a;
        }

        public int MultipleMethodsWithPostCall(int a, int b)
        {
            int c = a;
            if (a > 0)
            {
                if (foo(b) == 1)
                {
                    if(b == 5)
                    {
                        b--;
                    }
                    a++;
                }
                else if (foo(a) == 2)
                {
                    if(b == 5)
                    {
                        if(c == 1)
                        {
                            c = b;
                        }
                        a++;
                    }
                    b++;
                }
            }
            else
            {
                if (fun(a) == 3)
                {
                    return a + b;
                }
            }
            return a;
        }


        public int WhileLoop(int[] a, int b, char[] c)
        {
            while (true)
            {
                if (a[2] == 30)
                {
                    if (b == 0x5431)
                    {
                        if (c[0] == 1)
                        {
                            return 0;
                        }
                        return a[1];
                    }
                    else if (a[1] > a[2])
                    {
                        return a[2];
                    }
                    else
                    {
                        return b;
                    }
                }
                b++;
            }
        }

        public bool TestLoop1(int x, int[] y)
        {
            //Contract.Requires(y != null); Precondition P1
            //Contract.Requires(y.Length < 21); Precondition P2
            // Contract.Requires(y.Length < 20); Precondition P3
            if (x == 90)
            {
                for (int i = 0; i < y.Length; i++)
                {
                    if (y[i] == 15)
                    {
                        int p = 1;
                        for (int j = 0; j < i; j++)
                            if (y[j] == 16) p *= 2;
                        if (p > 10) x += p;
                    }
                }
                // Contract.Assert(x != 110); // The annotation Q.
                if (x == 90 + 32 + 16 + 64)
                    return true;
            }
            return false;
        }

        static void Main(string[] args)
        {
        }
    }
}
