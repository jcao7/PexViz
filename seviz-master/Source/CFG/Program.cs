﻿using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using SEViz.Common.Model;
using SEViz.Integration;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio;

namespace CFG
{

    public class Program : Package
    {
        public SEGraph MainGraph;
        public Dictionary<string, SEGraph> MethodGraphs;
        public Dictionary<string, List<string>> MethodParameters;
        private Random random;

        [STAThread]
        public static void Main()
        {
            Program a = new Program("Examples\\Program.cs", "MultipleMethodsWithPostCall");
            foreach(var g in a.MethodGraphs.Values)
            {
                Debug.WriteLine(String.Join(" : ",from r in g.returns select r.DisplayName));
            }
            // Checking if temporary SEViz folder exists
            if (!Directory.Exists(Path.GetTempPath() + "PexViz"))
            {
                var dir = Directory.CreateDirectory(Path.GetTempPath() + "PexViz");

            }

            // Getting the temporary folder
            var tempDir = Path.GetTempPath() + "PexViz\\";
            //Program.AttachFunctionCalls(a.MainGraph, a.MethodGraphs);
            // Serializing the graph into graphml
            SEGraph.Serialize(a.MainGraph, tempDir);
            //SEGraph a = SEGraph.Deserialize(tempDir + "temp.graphml");
        }

        public void generateControlFlowGraph(string className, string methodName)
        {
            var code = new StreamReader("..\\..\\..\\" + className).ReadToEnd();
            SyntaxTree tree = CSharpSyntaxTree.ParseText(code);
            var root = tree.GetRoot();
            var methods = root.DescendantNodes().OfType<MethodDeclarationSyntax>();

            foreach(var method in methods){

                Debug.WriteLine(method.Identifier);
                MethodParameters[method.Identifier.ToString()] = new List<string>(); 
                foreach(var p in method.ParameterList.Parameters)
                {
                    //Debug.WriteLine(p.Identifier.ToString());
                    MethodParameters[method.Identifier.ToString()].Add(p.Identifier.ToString());
                }
                SEGraph graph = new SEGraph();
                MethodGraphs[method.Identifier.ToString()] = graph;

                //Add root node
                SENode rootNode = new SENode(method.GetLocation().GetLineSpan().StartLinePosition.Line, "", method.Identifier.ToString(), "", "", "", false);
                graph.root = rootNode;
                rootNode.DisplayName = "Start";
                graph.AddVertex(rootNode);

                //Recursively construct graph
                SENode lastNode = rootNode;
                List<CSharpSyntaxNode> statements = getBranchStatement(method);
                List<SENode> curleaves = new List<SENode>();
                for (int i = 0; i < statements.Count; i++)
                {
                    CSharpSyntaxNode st = statements[i];
                    CSharpSyntaxNode prevst = i > 0 ? statements[i - 1] : null;

                    Tuple<SENode, List<SENode>> curt = buildGraph(prevst, st, graph,method.Identifier.ToString());
                    if (curt == null)
                    {
                        continue;
                    }
                    SENode newnode = curt.Item1;
                    List<SENode> newleaves = curt.Item2;

                    if (i == 0)
                    {
                        SEEdge eee = new SEEdge(random.Next(), "true", rootNode, newnode);
                        eee.MethodName = method.Identifier.ToString();
                        graph.AddEdge(eee);
                    }
                    string edgeName = "";
                    if (st is ElseClauseSyntax)
                    {
                        edgeName = "False";
                        curleaves.AddRange(newleaves);
                        SEEdge e = new SEEdge(random.Next(), edgeName, lastNode, newnode);
                        e.MethodName = method.Identifier.ToString();
                        graph.AddEdge(e);
                    }
                    else
                    {
                        foreach (SENode n in curleaves)
                        {
                            SEEdge ee = new SEEdge(random.Next(), "", n, newnode);
                            ee.MethodName = method.Identifier.ToString();
                            graph.AddEdge(ee);
                        }
                        curleaves = newleaves;
                    }

                    lastNode = newnode;
                }


                if (method.Identifier.ToString() == methodName)
                {
                    MainGraph = MethodGraphs[methodName];
                }
            }
            if(MainGraph == null){
                throw new ArgumentException("No such method: " + methodName);
            }

            

        }

        public static void  AttachFunctionCalls(SEGraph main, Dictionary<string, SEGraph> AllGraphs)
        {
            HashSet<int> ids = new HashSet<int>();
            HashSet<int> edges = new HashSet<int>();
            foreach (var v in main.Vertices)
            {
                ids.Add(v.Id);
            }
            foreach (var e in main.Edges)
            {
                edges.Add(e.Id);
            }
            Dictionary<string, SENode> methodStartNodes = new Dictionary<string, SENode>();
            foreach (var methodName in AllGraphs.Keys)
            {
                var g = AllGraphs[methodName];
                methodStartNodes[methodName] = g.root;
            }
            Random random = random = new Random();
            foreach (var methodName in AllGraphs.Keys)
            {
                var g = AllGraphs[methodName];
                if (g == main)
                {
                    foreach (var v in g.Vertices.ToList())
                    {
                        foreach (var methodName2 in methodStartNodes.Keys)
                        {
                            if (v.DisplayName.Contains(methodName2 + "("))
                            {
                                foreach (var node in AllGraphs[methodName].Vertices.ToList())
                                {
                                    if (!ids.Contains(node.Id))
                                    {
                                        main.AddVertex(node);
                                        ids.Add(node.Id);
                                    }
                                }
                                foreach (var edge in AllGraphs[methodName].Edges.ToList())
                                {
                                    if (!edges.Contains(edge.Id))
                                    {
                                        main.AddEdge(edge);
                                        edges.Add(edge.Id);
                                    }
                                }
                                foreach (var node in AllGraphs[methodName2].Vertices.ToList())
                                {

                                    //add node from method2 to main
                                    if (!ids.Contains(node.Id))
                                    {
                                        main.AddVertex(node);
                                        ids.Add(node.Id);
                                    }

                                    //if is return node, connect to all next node in method1
                                    if (AllGraphs[methodName2].returns.Contains(node))
                                    {
                                        var nexts = g.GetNeighborNodes(v);
                                        foreach (var nex in nexts)
                                        {
                                            if (v.MethodName == nex.MethodName)
                                            {
                                                SEEdge returnEdge = new SEEdge(random.Next(), node, nex);
                                                returnEdge.DisplayName = "Return";
                                                edges.Add(returnEdge.Id);
                                                main.AddEdge(returnEdge);
                                            }
                                        }
                                    }
                                }
                                foreach (var edge in AllGraphs[methodName2].Edges.ToList())
                                {
                                    if (!edges.Contains(edge.Id))
                                    {
                                        main.AddEdge(edge);
                                        edges.Add(edge.Id);
                                    }
                                }
                                SEEdge e = new SEEdge(random.Next(), v, methodStartNodes[methodName2]);
                                e.DisplayName = "Function Call";
                                edges.Add(e.Id);
                                main.AddEdge(e);
                                break;
                            }
                        }

                    }
                }
            }
        }
        public Program(string className, string methodName)
        {
            //string className = "SEViz.Common\\Model\\SEGraph.cs";
            //string methodName = "ReplaceLineBreaks";
            random = new Random();
            MethodGraphs = new Dictionary<string, SEGraph>();
            MethodParameters = new Dictionary<string, List<string>>();
            generateControlFlowGraph(className, methodName);
            AttachFunctionCalls(MainGraph, MethodGraphs);
        }
        private bool isLeaf(CSharpSyntaxNode n)
        {
            IEnumerable<SyntaxNode> holder = n.ChildNodes();
            if (holder.OfType<BlockSyntax>().Count() != 0)
            {
                holder = holder.OfType<BlockSyntax>().First().ChildNodes();
            }

            if(holder.Count() != 0 && ((holder.Last() is IfStatementSyntax && ((IfStatementSyntax)holder.Last()).Else != null) || (holder.Last() is ReturnStatementSyntax)))
            {
                return false;
            }
            return true;
        }

        private string IsLoopSyntax(CSharpSyntaxNode syntax)
        {
            if(syntax is WhileStatementSyntax)
            {
                return "while";
            }else if (syntax is ForStatementSyntax)
            {
                return "for";
            }else if (syntax is ForEachStatementSyntax)
            {
                return "foreach";
            }else 
            {
                return "false";
            }
        }
        public List<CSharpSyntaxNode> getBranchStatement(CSharpSyntaxNode s)
        {
            List<CSharpSyntaxNode> ret = new List<CSharpSyntaxNode>();

            IEnumerable<SyntaxNode> a = s.ChildNodes();
            //Debug.WriteLine("--"+s.GetType().ToString()+"---");
            //Debug.WriteLine("--start---");
            //foreach(SyntaxNode c in a)
            //{
            //    Debug.WriteLine("---");
            //    Debug.WriteLine(c.ToString());
            //}
            //Debug.WriteLine("--end---");
            IEnumerable<SyntaxNode> b =a.OfType<BlockSyntax>();
            IEnumerable<SyntaxNode> holder;
            if (b.Count() == 0)
            {
                holder = a;
            }else
            {
                holder = b.First().ChildNodes();
            }
            //Debug.WriteLine("--start---");
            foreach (CSharpSyntaxNode st in holder)
            {
                //Debug.WriteLine("---");
                //Debug.WriteLine(st.ToString());
                if (st is IfStatementSyntax)
                {
                    //Debug.WriteLine("Added if");
                    IfStatementSyntax ifst = ((IfStatementSyntax)st); 
                    ret.Add(ifst);
                    if (ifst.Else != null)
                    {
                        //Debug.WriteLine("Added else");
                        ret.Add(ifst.Else);
                    }
                    
                }
                if(IsLoopSyntax(st) != "false")
                {
                    //Debug.WriteLine("Added loop");
                    ret.Add((StatementSyntax)st);
                }
            }
            //Debug.WriteLine("--end---");
            return ret;
        }

        private bool HasReturnStatement(CSharpSyntaxNode n)
        {
            var returns = n.DescendantNodes().OfType<ReturnStatementSyntax>();
            if(returns.Count() == 0)
            {
                return false;
            }
                return true;
        }
        public Tuple<SENode, List<SENode>> buildGraph(CSharpSyntaxNode prevs,CSharpSyntaxNode s, SEGraph graph, string method)
        {
            int id = s.GetLocation().GetLineSpan().StartLinePosition.Line + s.GetType().GetHashCode();
            SENode rootNode = new SENode(id, "",method, "", "", "", false);
            rootNode.Color = SENode.NodeColor.Red;
            if (IsLoopSyntax(s) == "for")
            {
                
                rootNode.DisplayName = ((ForStatementSyntax)s).Condition.GetText().ToString();

            }
            else
                rootNode.DisplayName = s.DescendantNodes().OfType<ExpressionSyntax>().FirstOrDefault().GetText().ToString();



            if (!(s is ElseClauseSyntax))
            {
                graph.AddVertex(rootNode);
                //Debug.WriteLine(s.ToString());
                //Debug.WriteLine("Added node " + rootNode.DisplayName);
            }

            List<SENode> curleaves = new List<SENode>();
            List<CSharpSyntaxNode> statements = getBranchStatement(s);
            if(!(s is ElseClauseSyntax) && statements.Count == 0 && HasReturnStatement(s))
            {
                graph.returns.Add(rootNode);
            }
            //Debug.WriteLine("--start---");
            //foreach (SyntaxNode c in statements)
            //{
            //    Debug.WriteLine("---");
            //    Debug.WriteLine(c.ToString());
            //}
            //Debug.WriteLine("--end---");
            SENode lastNode = rootNode;
            bool rootIsLeaf = false;
            for(int i = 0; i < statements.Count; i++)
            {
                CSharpSyntaxNode st = statements[i];
                CSharpSyntaxNode prevst = i > 0 ? statements[i - 1] : null;
                
                Tuple<SENode, List<SENode>> curt = buildGraph(prevst,st, graph,method);
                if(curt == null)
                {
                    continue;
                }
                SENode newnode = curt.Item1;
                List<SENode> newleaves = curt.Item2;

                if (i == 0)
                {
                    if(s is ElseClauseSyntax)
                    {
                        rootNode = newnode;
                        lastNode = newnode;
                    }else
                    {
                        SEEdge eee = new SEEdge(random.Next(), "True", rootNode, newnode);
                        eee.MethodName = method;
                        graph.AddEdge(eee);
                    }
                    
                }
                //Debug.WriteLine(newleaves.Count);
                string edgeName = "";
                if (st is ElseClauseSyntax)
                {
                    if (graph.Vertices.Contains(newnode))
                    {
                        edgeName = "False";
                        curleaves.AddRange(newleaves);
                        SEEdge e = new SEEdge(random.Next(), edgeName, lastNode, newnode);
                        e.MethodName = method;
                       // Debug.WriteLine(lastNode.DisplayName + " to " + newnode.DisplayName);
                        graph.AddEdge(e);
                    }else if (newleaves.Count != 0)
                    {
                        rootIsLeaf = true;
                    }
                }
                else
                {
                    foreach (SENode n in curleaves)
                    {
                        SEEdge ee = new SEEdge(random.Next(), "False", n, newnode);
                        ee.MethodName = method;
                        //Debug.WriteLine(st.ToString());
                        //Debug.WriteLine(n.DisplayName + " to " + newnode.DisplayName);
                        graph.AddEdge(ee);
                    }
                    curleaves = newleaves;
                }
                
                lastNode = newnode;
            }
            if (IsLoopSyntax(s) != "false")
            {
                //rootNode.Type = SENode.NodeType.Loop; 
                foreach(SENode n in curleaves)
                {
                    SEEdge e = new SEEdge(random.Next(), "Loop", n, rootNode);
                    e.MethodName = method;
                    graph.AddEdge(e);
                }
                curleaves = new List<SENode>();
                rootIsLeaf = true;
            }else
            {
                //rootNode.Type = SENode.NodeType.If;
            }
            Tuple<SENode, List<SENode>> t;
            //Debug.WriteLine("test for leaf"+s.ToString());
            if (isLeaf(s) || rootIsLeaf)
            {
                if(!curleaves.Contains(rootNode))
                curleaves.Add(rootNode);

            }else if (s is ElseClauseSyntax && statements.Count == 0)
            {
                //Debug.WriteLine("null: " + rootNode.DisplayName);
                return null;
            }

            t = new Tuple<SENode, List<SENode>>(rootNode, curleaves);
            return t;
        }
    }
}