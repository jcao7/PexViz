﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    public class A
    {
        public int Foo(bool a, int b)
        {
            if (a) return Bar(b);
            else return -1;
        }

        public int Bar(int b)
        {
            if (b > 0) return 1;
            else return 0;
        }

        public bool TestLoop1(int x, int[] y)
        {
            //Contract.Requires(y != null); Precondition P1
            //Contract.Requires(y.Length < 21); Precondition P2
            // Contract.Requires(y.Length < 20); Precondition P3
            if (x == 90)
            {
                for (int i = 0; i < y.Length; i++)
                {
                    if (y[i] == 15)
                    {
                        int p = 1;
                        for (int j = 0; j < i; j++)
                            if (y[j] == 16) p *= 2;
                        if (p > 10) x += p;
                    }else
                    {
                        if(x>0)
                        x++;
                    }
                }
                // Contract.Assert(x != 110); // The annotation Q.
                if (x == 90 + 32 + 16 + 64)
                    return true;
            }
            return false;
        }
        public int Loop(int[] a, int b)
        {
            while (true)
            {
                if (a[2] == 30)
                {
                    if (b == 0x5431)
                    {
                        return a[1];
                    }
                    else if (a[1] > a[2])
                    {
                        return a[2];
                    }
                    else
                    {
                        b++;
                    }
                }else if(a[1] == 10)
                {
                    a[0]++;
                }
                if(a[1] == 1)
                {
                    a[1]++;
                }else
                {
                    b--;
                }
                b++;
            }
        }
    }
}
